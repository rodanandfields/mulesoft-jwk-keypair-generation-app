# JWT Generator App

## Purpose
This app uses the JWKS Generation Module (https://bitbucket.org/rodanandfields/mulesoft-jwks-module) to generate the RSA public and private keys and upload it to 
Rodan and Fields Hashicorp Vault.

This is a pre-requisite to construct a JWT using the keys from the Hashicorp Vault.

### Scheduler to generate JWKS
The scheduler is supposed to run every 240 days ( 6 months as defined by the security team) to generate the RSA keypair and upload it to vault.
The frequency can be changed as it is a configuration property.

## Usage
This application can be invoked in real time using the endpoint: https://mulesoft-jwks-generation-app.us-w1.cloudhub.io/jwk-key-pair


The properties in the configuration needed to run this application are as follows:

## Configuration

The role id and secret id are the credentials for the Vault Login API:
role_id=35ab6f04-cef9-dee6-77a7-74b512148134
secret_id=da5be3c9-049e-bbb3-72d2-d6bb6430e974

Frequency to run the scheduler at a frequency:

frequencyindays=240

Vault URL:

vault_url=https://vault.rodanandfields.com